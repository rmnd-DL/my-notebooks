import cv2
import torch
from torchvision import transforms as T
from torchvision. transforms import functional as F
import albumentations as A
from albumentations.pytorch import ToTensorV2
from tabulate import tabulate

backbone_model_path =  'ArcFace_efficientnet_b2.pth'

num_of_classes = 760

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)

album_data_normalization = A.Compose([
    A.ToFloat(),
    ToTensorV2()
],)

torch_data_normalization = T.Compose([T.ToTensor(),])


model = torch.load(backbone_model_path, map_location=device).to(device)

model.eval()
img = cv2.imread("./images/res/1.jpg")
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

with torch.no_grad():

    inputs = []

    inputs.append(F.to_tensor(img))
    inputs.append(album_data_normalization(image=img)['image'])
    inputs.append(torch_data_normalization(img))

    input_tensor = torch.stack(inputs, dim=0)

    res = model(input_tensor)
    print(tabulate(
        res[:, :4].T.tolist(), # Cut all values except first 4 per each item
        headers=["TV Functional", "TV Compose", "Album Compose"]))


"""
OUTPUT
  TV Functional    TV Compose    Album Compose
---------------  ------------  ---------------
       31.0357       31.0357          31.0357
        2.66535       2.66535          2.66535
       15.8527       15.8527          15.8527
        8.29196       8.29196          8.29196

"""