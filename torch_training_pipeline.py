import os
import gc
import time
# import random
import typing as t
from copy import deepcopy
# Classic packages for data manipulation and visualization
import numpy as np
import pandas as pd
# import matplotlib.pyplot as plt # Utils
import joblib  # Pipelining, pickling (dump/load), parallel processing
from tempfile import TemporaryDirectory
# Output text colorizing
from colorama import Back, Style


def print_highlighted(text: str, bgcolor=Back.GREEN) -> None:
    """
    Function to print a text with colored background.
    """
    print(bgcolor + text + Style.RESET_ALL)


from torchmetrics.classification import MulticlassF1Score # F1 metric for multiclass
# Classic ML tools
from sklearn.preprocessing import LabelEncoder


from time import time
from tqdm import tqdm  # Progress bar for training process
import torch
# from torch import nn  # I use particular imports to save memory
from torch.nn import CrossEntropyLoss, Linear, Sequential, SiLU, BatchNorm2d, Dropout, Module
from torch.utils.data import DataLoader, Dataset
# from torchvision.ops import Conv2dNormActivation
from torchvision.models import get_model
import albumentations as A
from albumentations.pytorch import ToTensorV2
from PIL import Image


import wandb
wandb.login()  # We log in via pop-up


device = torch.device(
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print(f"Device is: {device}")


run = wandb.init(project="Car color classification with SOTA CNN Models",
    config={
        "seed": 2306,
        "epochs": 20,
        "image_dimension": 256,  # Depends on pretrained model used
        "model_name": "efficientnet_b0",  # Pretrained model we will use
        "embedding_size": 512,  # Embedding output size
        "batch_size": 64,
        "learning_rate": 25e-4,
        "min_lr": 1e-8,
        "min_loss_delta": 1e-7,  # To stop training on plateau
        "weight_decay": 1e-7,
        "allow_swap_memory": True,  # Allow to allocate unlimited memory while on Apple Metal
        "image_extension": '.jpg',
    },
    name="30k images training",  # Name of run (dashboards group)
)
config = run.config
np.random.seed(config.seed)
torch.manual_seed(config.seed)


config.data = '../data/DVM_dataset/30k_dataset/'
config.train_images_path = config.data + '/train'
config.test_images_path = config.data + '/test'

config.train_labels = config.data + 'train_labels.csv'
config.test_labels = config.data + 'test_labels.csv'
config.save_model_to = './saved_instances/'
config.save_history_to = './saved_instances/cars_DMV_training_history.pkl'
os.makedirs(config.save_model_to, exist_ok=True)

config.model_path = "./saved_instances/efficientnet_b0_model_DVM_10k.pth"
config.model_weights = "./saved_instances/car_color_model_RGBA.pt"


def get_file_path_by_id(file_id, dir="./"):
    return os.path.join(dir, str(file_id) + config.image_extension)

def apply_label_encoding(data: t.Union[pd.DataFrame, np.array],
                         encoder_name: os.path,
                         column='label', action='encode',
                         ):
    """
    One Hot encoding. We apply encoding by replacing the label column in dataframe.
    As for decoding data back, we work with vector-array (as it's most likely to
    be a prediction result)
    """
    encoder = LabelEncoder()
    if action == 'encode':
        # We transform dataframe here. Nothing returns
        data[column] = encoder.fit_transform(data[column])
        with open(
                os.path.join(config.save_model_to, f"{encoder_name}_LEncoder.pkl"), "wb") as fp:
            joblib.dump(encoder, fp)
    elif action == 'decode':
        # We pass vector here. Result is a vector
        with open(
                os.path.join(config.save_model_to, f"{encoder_name}_LEncoder.pkl"), "rb"
        ) as fp:
            encoder: LabelEncoder = joblib.load(fp)
        if type(data) == torch.Tensor:
            data = data.cpu().numpy()
        return encoder.inverse_transform(data)


class CustomImagesDataset(Dataset):
    """
    """
    def __init__(self, data: pd.DataFrame=None, images_path: os.path=None, labels_path:os.path=None, transform_images: A.Compose=None, encoder_name: str=None):
        """

        """
        super().__init__()
        assert (data is not None) or (labels_path is not None and images_path is not None)

        if data is None:
            data = pd.read_csv(labels_path)
            data['file_path'] = data['id'].apply(get_file_path_by_id, dir=images_path)

        self.images_paths = data['file_path'].values
        self.encoder_name = encoder_name if encoder_name else self.__hash__()  # We use hash as a unique name
        print(f"Label Encoder saved with id `{self.encoder_name}`")
        apply_label_encoding(data=data, action='encode', encoder_name=self.encoder_name)
        self.labels = data['label'].values
        #         self.indexes = data['id'].values
        self.transform_images = transform_images
        self.__set_dataset_len()

    def __set_dataset_len(self):
        self.length = self.labels.shape[0] # Number of rows

    def __len__(self):
        """
        We calculate the len in another function, so that we are able to slice.
        """
        return self.length

    def decode_labels(self, labels_vector):
        return apply_label_encoding(data=labels_vector, action='decode', encoder_name=self.encoder_name)

    def __getitem__(self, index) -> tuple[torch.Tensor, int]:
        """ Function to return item by indexing the dataset """

        if not isinstance(index, int) and isinstance(index, slice):
            # It's not an index, but slice.
            # We will return the part of data by making a copy of the dataset
            index: slice
            self = deepcopy(self)
            self.length = index.stop  # Cut the length of dataset.
            self.labels = self.labels[:self.length]
            return self
        assert self.__len__() >= index

        image = Image.open(self.images_paths[index])
        if self.transform_images:
            # Albumentations requires us to convert image to Numpy Array
            image = self.transform_images(image=np.array(image))['image']

        label = self.labels[index]
        return image, label

    @property
    def classes_(self):
        return self.decode_labels(np.unique(self.labels))


data_transforms = {
    "train": A.Compose([
        A.Resize(config.image_dimension, config.image_dimension),
        #         A.RandomCrop(width=224, height=224, p=1),
        # A.OneOf([
        #     A.Resize(224, 224, p=0.5),
        #     A.RandomCrop(width=224, height=224, p=1.0),
        # ], p=1.0),
        # A.ShiftScaleRotate(
        #     shift_limit=0.15,
        #     scale_limit=0.01,
        #     rotate_limit=30,
        #     p=0.5
        # ),
        # A.HueSaturationValue(
        #     hue_shift_limit=30,
        #     sat_shift_limit=30,
        #     val_shift_limit=30,
        #     p=0.5
        # ),
        # A.RandomBrightnessContrast(
        #     brightness_limit=(-0.01,0.01),
        #     contrast_limit=(-0.01, 0.01),
        #     p=0.5
        # ),
        A.Normalize(
            mean=0.5,
            std=0.5,
            max_pixel_value=255.0,
            p=1.0
        ),
        # A.ToFloat(max_value=255.0),  # If we don't apply Normalization, WE MUST TURN IT TO FLOAT
        ToTensorV2(),  # Advanced analogue of T.ToTensor
    ], p=1.),

    "test": A.Compose([
        A.Resize(config.image_dimension, config.image_dimension),
        # Use either Normalize or ToFloat

        A.Normalize(
            mean=0.5,
            std=0.5,
            max_pixel_value=255.0,
            p=1.0
        ),
        # A.ToFloat(max_value=255.),
        ToTensorV2(),
    ], p=1.)
}

train_dataset = CustomImagesDataset(
    labels_path=config.train_labels, images_path=config.train_images_path,
    transform_images=data_transforms['train'], encoder_name='train'
)
test_dataset = CustomImagesDataset(
    labels_path=config.test_labels, images_path=config.test_images_path,
    transform_images=data_transforms['test'], encoder_name='test',
)

config.num_of_classes = len(train_dataset.classes_)
print(f"Number of classes: {config.num_of_classes}")

if config.allow_swap_memory:
    os.environ['PYTORCH_MPS_HIGH_WATERMARK_RATIO'] = '0.0'  # Allow swap
    print("WARNING: Using of swap memory was allowed.")


# Define the data loaders
train_loader = DataLoader(train_dataset, batch_size=config.batch_size, shuffle=True,)
test_loader = DataLoader(test_dataset, batch_size=config.batch_size * 2, shuffle=False,)


def get_input_feature_size(classifier: Sequential) -> int:
    for module in classifier.modules():
        if isinstance(module, Linear):
            return module.in_features

def get_torch_model(from_model_name=None, from_path=None, pretrained=False,
                    freeze_layers=0.0, dropout=0.1,
                    get_embeddings: bool = False,
                    transform_classifier: bool = False,
                    to_device: t.Union[str, torch.device] = 'cpu',
                    ) -> torch.nn.Module:
    print(f"Loading model {from_model_name or from_path}")
    if from_path:
        try:
            # Attempt to load whole model
            model = torch.load(from_path, map_location=to_device)
        # except AttributeError:
        #     # It is likely not a .pth but a .pt file, i.e. weights
        #     model = torch.load(from_path, map_location=to_device)
        except FileNotFoundError:
            raise

    elif from_model_name:
        model = get_model(from_model_name, weights="DEFAULT" if pretrained else None)

    if bool(freeze_layers):
        params = list(model.parameters())
        frozen = 0
        for param in params[:int(len(params)*freeze_layers)]:
            # Freeze some layers
            param.requires_grad = False
            frozen += 1
        print_highlighted(f"{frozen} of {len(params)} model parameters were frozen.")

    if get_embeddings:
        # That means remove classifier (last layer):
        model.classifier = Sequential(
            Dropout(p=dropout, inplace=True),
            Linear(
                in_features=get_input_feature_size(model.fc),
                out_features=config.embedding_size, bias=False
            ),
        )
    elif transform_classifier:
        try:
            output_layer = model.classifier
        except AttributeError:
            output_layer = model.fc

        model.classifier = Sequential(
            Linear(in_features=get_input_feature_size(output_layer), out_features=config.num_of_classes, bias=True),
            Dropout(p=dropout, inplace=True),
            #             nn.Softmax(dim=-1)
        )
    return model.to(device)


model = get_torch_model(  # To use new pretrained model
    from_model_name=config.model_name,
    pretrained=False,
    freeze_layers=0.7,
    transform_classifier=True,
    to_device=device
)

# model = get_torch_model(  # To use previously saved instance
#     from_path=config.model_path,
#     pretrained=False,
#     freeze_layers=0.75,
#     to_device=device
# )

# model.features[0] = Conv2dNormActivation(  # Transform input layer, so it will accept 4 channels (RGBA)
#     4, model.features[0][0].out_channels, kernel_size=3, stride=2,
#     norm_layer=BatchNorm2d, activation_layer=SiLU
# )
# model.load_state_dict(torch.load(config.model_weights, map_location=device))
# model.classifier = Sequential(
#     Linear(in_features=get_input_feature_size(model.classifier), out_features=config.num_of_classes, bias=True),
#     Dropout(p=0.2, inplace=True),
# )
# model.to(device)

run.watch(model)
model_artifact = wandb.Artifact(
    config.model_name, type="model",
    metadata={'Number of params': str(sum([np.prod(p.size()) for p in model.parameters()]))}
)
wandb.log_artifact(model_artifact)


f1_score = MulticlassF1Score(num_classes=config.num_of_classes, average="micro").to(device)



# Define the loss function and optimizer
criterion = CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=config.learning_rate)
# scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=0.5, verbose=False)
scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
    optimizer, 'min', patience = 3, factor=0.5,
    min_lr=config.min_lr, verbose=False,
    threshold=config.min_loss_delta
)


def train_one_epoch(model, dataloader, optimizer, criterion, device, epoch):
    model.train()
    train_loss = 0.0
    train_correct = 0.0
    train_f1 = 0.0
    n_batches = 0
    data_size = 0

    with tqdm(dataloader, desc=f'Epoch: {epoch}', unit="batch") as tepoch:
        for images, labels in tepoch:
            n_batches += 1
            images = images.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()

            logits = model(images)
            del images
            loss = criterion(logits, labels)
            loss.backward()

            optimizer.step()

            gc.collect()

            loss = loss.item()  # item() extracts scalar
            train_loss += loss
            _, predicted = torch.max(logits, 1)
            del logits

            train_correct += (predicted == labels).sum().item()
            data_size += labels.size(0)
            train_accuracy = train_correct / data_size

            f1 = f1_score(predicted, labels).item()
            train_f1 += f1
            tepoch.set_postfix(loss=loss, acc=train_accuracy)

    train_loss /= n_batches
    train_f1 /= n_batches
    gc.collect()
    # torch.cuda.empty_cache()

    return train_loss, train_accuracy, train_f1



@torch.inference_mode()
def validate_one_epoch(model, dataloader, criterion, device):
    model.eval()

    val_loss = 0.0
    val_correct = 0.0
    val_f1_score = 0.0
    data_size = 0
    n_batches = 0

    with tqdm(dataloader, desc="Validation",unit="batch") as tepoch:
        for images, labels in tepoch:
            n_batches += 1
            images = images.to(device)
            labels = labels.to(device)
            output = model(images)
            del images
            loss = criterion(output, labels).item()
            val_loss += loss
            _, predicted = torch.max(output, 1)

            correct = (predicted == labels).sum().item()
            val_correct += correct
            data_size += labels.size(0)
            val_accuracy = val_correct / data_size

            f1 = f1_score(predicted, labels).item()
            val_f1_score += f1
            tepoch.set_postfix(loss=loss, acc=val_accuracy)

    val_loss /= n_batches
    val_f1_score /= n_batches
    gc.collect()
    # torch.cuda.empty_cache()

    return val_loss, val_accuracy, val_f1_score


def get_lr(optimizer):
    """
    For some schedulers we don't have `get_last_lr()` method.
    So, we will get last lr ourselves.
    """
    return tuple(
        [group['lr'] for group in optimizer.param_groups]
    )


def manipulate_training_history(action: t.Literal["put", "get"], data=None) -> t.Union[None, dict]:
    if action == 'put':
        with open(
                config.save_history_to, "wb"
        ) as fp:
            joblib.dump(data, fp)
            print("Training history was saved to: ", config.save_history_to)
    elif action == 'get':
        try:
            with open(
                    config.save_history_to, "rb"
            ) as fp:
                data: dict = joblib.load(fp)
        except FileNotFoundError:
            raise
        return data
    else: raise ValueError

def train_model(model, train_loader, val_loader,
                criterion=CrossEntropyLoss(), optimizer=torch.optim.Adam(model.parameters()),
                num_epochs=5, scheduler=None,
                device=torch.device("cpu"),
                save_model_to=f"./{config.save_model_to}/{config.model_name}_model.pth", save_parameters_to=f"./{config.save_model_to}/{config.model_name}_weights.pt",
                start_from_checkpoint=False,
                ) -> t.Tuple[Module, dict]:
    since = time()
    try:
        history = None
        if start_from_checkpoint:
            try:
                history = manipulate_training_history("get")
                for k, metric in history.items():
                    # Log previous scores
                    [run.log({k: m}) for m in metric]

                print_highlighted("Successfully uploaded history from the checkpoint.")
            except FileNotFoundError:
                print_highlighted("Checkpoint wasn't found. Starting from scratch...")
        if not start_from_checkpoint or history is None:
            history = dict(
                train_loss=[],
                valid_loss=[],
                train_accuracy=[],
                valid_accuracy=[],
                train_f1_score=[],
                valid_f1_score=[],
            )

        with TemporaryDirectory() as tempdir:  # Folder for weights checkpoints
            best_model_params_path = os.path.join(tempdir, 'best_model_params.pt')

            for epoch in range(num_epochs):
                train_loss, train_accuracy, train_f1 = train_one_epoch(model, train_loader, optimizer, criterion, device, epoch=f'{epoch+1}/{num_epochs}',)
                val_loss, val_accuracy, val_f1 = validate_one_epoch(model, val_loader, criterion, device,)


                print_highlighted(
                    f"Loss: {val_loss:.4f} - Accuracy: {val_accuracy:.4f} - F1: {val_f1:.4f} - LR: {get_lr(optimizer)}"
                )

                if not history["valid_loss"] or val_loss < history["valid_loss"][-1]:
                    # If it's either first step (history is empty) or our weights are getting better
                    print_highlighted("New weights were applied.\n", bgcolor=Back.BLUE)
                    torch.save(model.state_dict(), best_model_params_path)

                scheduler.step(val_loss)  # Use it if your scheduler work is based on loss value
                # scheduler.step()

                history["train_loss"].append(train_loss)
                history["valid_loss"].append(val_loss)
                history["train_accuracy"].append(train_accuracy)
                history["valid_accuracy"].append(val_accuracy)
                history["train_f1_score"].append(train_f1)
                history["valid_f1_score"].append(val_f1)
                run.log({  # Log last scores from history
                        k: v for k, v in map(
                            lambda x: (x[0], x[1][-1]),
                            history.items())
                })
                gc.collect()
                # torch.cuda.empty_cache()

            time_elapsed = time() - since
            print_highlighted(f'Training completed in {time_elapsed // 60:.0f}m {time_elapsed % 60:.0f}s')


            try:  # Load best model weights
                model.load_state_dict(torch.load(best_model_params_path))
            except FileNotFoundError:
                # Nothing to load, skip that step
                pass

            if save_model_to:
                torch.save(model, save_model_to)
                print(f"Model was saved to: {save_model_to}")
            if save_parameters_to:
                torch.save(criterion.state_dict(), save_parameters_to)
                print(f"Weights were saved to: {save_parameters_to}")

            manipulate_training_history("put", history)

        return model
    except KeyboardInterrupt:
        time_elapsed = time() - since
        print_highlighted(
            f"Training was force aborted after {time_elapsed // 60:.0f}m {time_elapsed % 60:.0f}s "
            f"of training at epoch {epoch}.\n", bgcolor=Back.RED
        )
        save_model = int(input("Do you want to save model? Input 0 for No, any number for Yes."))
        if save_model:
            torch.save(model, save_model_to)
            print(f"Model was saved to: {save_model_to}")

            torch.save(criterion.state_dict(), save_parameters_to)
            print(f"Weights were saved to: {save_parameters_to}")
            manipulate_training_history("put", history)
        print_highlighted("Program was terminated.", bgcolor=Back.RED)
        exit()



if __name__ == '__main__':
    model = train_model(
        model=model,
        train_loader=train_loader,
        val_loader=test_loader,
        criterion=criterion,
        optimizer=optimizer,
        num_epochs=config.epochs,
        device=device,
        scheduler=scheduler,
        save_model_to=f"{config.save_model_to}{config.model_name}_model_DVM_30k.pth",
        save_parameters_to=f"{config.save_model_to}{config.model_name}_weights_DVM_30k.pt",
        start_from_checkpoint=False,

    )

# ML Metrics
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report



def eval_model(model):
    model.eval()  # Set the model to evaluation mode
    predictions = []
    correct = 0
    total = 0

    with torch.no_grad():
        with tqdm(test_loader, desc="Final evaluating...", unit="batch") as process:
            for images, labels in process:
                images = images.to(device)
                labels = labels.to(device)
                output = model(images)
                _, predicted = torch.max(output, 1)
                correct += (predicted == labels).sum().item()
                total += labels.size(0)
                predictions.extend(predicted.cpu().numpy())
                del images
                del labels
                del output
    print_highlighted(
        f"Manually calculated accuracy: {correct/total};\n"
        f"Sklearn Accuracy: {accuracy_score(test_dataset.labels, np.array(predictions))}."
    )
    c_report = classification_report(test_dataset.labels, np.array(predictions), output_dict=True, zero_division=0)
    c_report = pd.DataFrame(c_report).transpose()

    if 'Unnamed: 0' in c_report.columns:  # Sklearn saved indexes as Unknown col
        labels = test_dataset.decode_labels(c_report['Unnamed: 0'][:-3].values.astype(int)).tolist()  # Convert labels to Colors
        labels.extend(c_report['Unnamed: 0'][-3:])
        c_report.insert(0, 'label', labels)
        c_report.drop(columns=['Unnamed: 0',], inplace=True)

    else:  # Sklearn saved indexes as indexes
        labels = c_report.index
        new_labels = test_dataset.decode_labels(labels[:-3].values.astype(int)).tolist()  # Convert labels to Colors
        new_labels.extend(labels[-3:])
        c_report.insert(0, 'label', new_labels)

    report_file = "./DVM10k_Model_metrics.csv"
    c_report.to_csv(report_file, index=False)
    print(f"Report was saved to {report_file}")

eval_model(model)
